#ifndef GAME_H
#define GAME_H


#include <ncurses.h>
#include <cstdlib>
#include <chrono>
#include <algorithm>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <thread>
#include <mutex>

#include "dungeon.h"
#include "player.h"
#include "monster.h"
#include "object.h"


class Flags{
  public:
  int _NumberMonsters;
  int _Max_Rooms;
  bool _save;
  bool _load;
  bool _player_auto;
  int _player_speed;
  std::string _filename;
  bool _verbose;
  Flags();
  ~Flags();
  Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms);
  Flags(const Flags& f);
};


class Game{
  


public:
  Game();
  Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon,bool checkParser,std::string mfilename, std::string ofilename, int max_rooms);
  ~Game();
  void gameLoop();
  bool ParseIsGood;
  
private:

  bool checkParse;
  std::string _mFile;
  std::string _oFile;
  console_t* console;
  void gameInit();
  unsigned int getElapsedTime();
  Dungeon *d;
  player_t* p;
  bool _reveal;
  std::vector<character_t*> mList;
  std::vector<monster_t> monsterTypes;
  std::vector<object> objectTypes;
  std::chrono::time_point<std::chrono::system_clock> startTime;
  void initAllColors();
  Flags* flag;
  void generateMonsters();
  void deleteMonsters();
  void renderMonsters();
  void resetEventList();
  void GameOverScreen();
  void printMonsterList();
  void resetMap();
  void generateObjects();
  void renderObjects();
  void renderMap();
  void teleportMode();
  void Render();
  void listInput();
  
  bool teleporting;
  bool displayMonsterList;
  bool Rendering;
  int parseFile(std::string mfilename, std::string ofilename);
  int parseMonsters(std::string mfilename);
  int parseObjects(std::string ofilename);
  std::vector<character_t*> eventQueue;
  position_t teleport;
  int cursorLocation;
  int listStart;
  WINDOW* monsterList;
  int mList_W;
  int mList_H;
  std::mutex mtx;
  
  
  typedef std::map<std::pair<int,int>,character_t*> cMap;
  typedef std::pair<std::pair<int,int>,character_t*> cPair; 
  typedef std::pair<cMap::iterator,bool> cMapItr; 
  cMap character_map;
  cMapItr insertCharacter(position_t pos, character_t* c);
  void eraseCharacter(position_t pos);
  
  typedef std::map<std::pair<int,int>,std::vector<object>> oMap; 
  typedef std::pair<std::pair<int,int>,std::vector<object>> oPair;
  typedef std::pair<oMap::iterator,bool> oMapItr;
  oMap object_map;
  oMapItr insertObject(position_t pos, object obj);
  std::vector<object> eraseObject(position_t pos);
  oMapItr checkObject(position_t pos);
  
  
  
protected: 
  
  
};

#endif