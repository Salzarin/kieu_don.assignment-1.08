#include "game.h"



Flags::Flags(){
  
  _NumberMonsters = 5;
  _Max_Rooms = 5;
  _save = false;
  _load = false;
  _player_auto = false;
  _player_speed = 10;
  std::string _filename = "";
}

Flags::~Flags(){
  
  
}

Flags::Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms){
  _NumberMonsters = nummon;
  _Max_Rooms = max_rooms;
  _save = s;
  _load = l;
  _player_auto = a;
  _player_speed = ps;
  _verbose = v;
  _filename = fn;
}

Flags::Flags(const Flags& f){
    _NumberMonsters = f._NumberMonsters;
  _Max_Rooms = f._Max_Rooms;
  _save = f._save;
  _load = f._load;
  _player_auto = f._player_auto;
  _player_speed = f._player_speed;
  _verbose = f._verbose;
  _filename = f._filename;
}

Game::Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, bool checkParser,std::string mfilename, std::string ofilename, int max_rooms) : Game(){
  
  if(!checkParser){
    checkParse = false;
    flag = new Flags(fn,v,l,s,a,ps,nummon,max_rooms);
    _mFile = mfilename;
    _oFile = ofilename;
    ParseIsGood = parseFile(_mFile, _oFile);
    if(ParseIsGood){
      gameInit();
    }
  }
  else{
    checkParse = true;
    parseFile(mfilename, ofilename);
  }
  
}


Game::Game(){
  

}

Game::~Game(){
  delwin(monsterList);
  delete d;
  delete p;
  delete console;
  delete flag;
  deleteMonsters();
  endwin();
}

unsigned int Game::getElapsedTime(){  
  return std::chrono::duration_cast<std::chrono::seconds>
  (std::chrono::system_clock::now() - startTime).count();
}

void Game::deleteMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    delete (*itr);
    (*itr)=NULL;
  }
  mList.clear();
}

int Game::parseMonsters(std::string mfilename){

  std::string _mfilename = mfilename;
  
  std::cout<<std::endl<<std::endl;
  if(_mfilename.length() == 0){
    _mfilename = std::string(getenv("HOME"))+
		     std::string("/.rlg327/")+
		     std::string("monster_desc.txt");
  }
  std::cout<< "Loading file " << _mfilename <<std::endl;
  
  std::ifstream mloadfile(_mfilename.c_str());
  
  if(!_mfilename.length()){
    printf("No File");
    return -1;
  }
  if(!mloadfile.good()){
    printf("File Error : %s : %s\n", _mfilename.c_str(),strerror(errno));
    return -1;  
  }
  std::string temp;
  std::getline(mloadfile,temp);
  if(temp.compare(std::string("RLG327 MONSTER DESCRIPTION 1\n"))){
    
    //std::cout<<"Monster Data Found"<<std::endl;
    
    int na, de, co, sp, ab, h, da, sy;
    int numerrors = 0;
    int numcorrect = 0;
    while(!mloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN MONSTER");
      na = de = co = sp = ab = h = da = sy=0;
      std::string name, desc, color, speed, abil, hp, dam, symbol;
      while(i == -1){
	std::getline(mloadfile,temp);
	
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(mloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(mloadfile,temp);
	      
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("SYMB") == 0){
	  symbol =  getParsedData(temp, std::string("SYMB"),sy);
	  sy++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ABIL") == 0){
	  abil =  getParsedData(temp, std::string("ABIL"),ab);
	  ab++;
	}
	if(temp.find("HP") == 0){
	  hp =  getParsedData(temp, std::string("HP"),h);
	  h++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	i = temp.find("END");
	j = temp.find("BEGIN MONSTER");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*co*sp*ab*h*da*sy==1){
	monsterTypes.push_back(monster_t(name,desc,symbol,color,speed,abil,hp,dam));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
    //std::cout<<"Checking Next Monster"<<std::endl<<std::endl;
    std::getline(mloadfile,temp);
      
      
    }
    std::cout<<"Found "<<numcorrect<<" Monsters and "<<numerrors << " Parsing Errors."<<std::endl; 
    if(checkParse){
      
      std::cout<<"Checking Monster Parsing."<<std::endl<<std::endl;
      
      std::vector<monster_t>::iterator itr;
      
      for(itr=monsterTypes.begin();itr!=monsterTypes.end();itr++){
	(*itr).parseMonster();
      }    
    }
  }
  return 0;
}

int Game::parseObjects( std::string ofilename){
  
  std::cout<<std::endl<<std::endl;
  
  std::string _ofilename = ofilename;
  
  if(_ofilename.length() == 0){
    _ofilename = std::string(getenv("HOME"))+
		     std::string("/.rlg327/")+
		     std::string("object_desc.txt");
  }
  
  
  std::cout<< "Loading file " << _ofilename <<std::endl;
  
  std::ifstream oloadfile(_ofilename.c_str());
  
  if(!_ofilename.length()){
    printf("No File");
    return -1;
  }
  if(!oloadfile.good()){
    printf("File Error : %s : %s\n", _ofilename.c_str(),strerror(errno));
    return -1;  
  }
  std::string temp;
  std::getline(oloadfile,temp);
  if(temp.compare(std::string("RLG327 OBJECT DESCRIPTION 1\n"))){
    
    //std::cout<<"Object Data Found"<<std::endl;
    
    //std::vector<object> objects;
    int na,de,ty,co,hi,da,dod,d,we,sp,at,va;
    int numerrors = 0;
    int numcorrect = 0;
    while(!oloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN OBJECT");
      na=de=ty=co=hi=da=dod=d=we=sp=at=va = 0;
      std::string name, desc, type, color, hit, dam, dodge, def, weight, speed, attr, val;
      while(i == -1){
	std::getline(oloadfile,temp);
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(oloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(oloadfile,temp);     
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("TYPE") == 0){
	  type =  getParsedData(temp, std::string("TYPE"),ty);
	  ty++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("HIT") == 0){
	  hit =  getParsedData(temp, std::string("HIT"),hi);
	  hi++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	if(temp.find("DODGE") == 0){
	  dodge =  getParsedData(temp, std::string("DODGE"),dod);
	  dod++;
	}
	if(temp.find("DEF") == 0){
	  def =  getParsedData(temp, std::string("DEF"),d);
	  d++;
	}
	if(temp.find("WEIGHT") == 0){
	  weight =  getParsedData(temp, std::string("WEIGHT"),we);
	  we++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ATTR") == 0){
	  attr =  getParsedData(temp, std::string("ATTR"),at);
	  at++;
	}
	if(temp.find("VAL") == 0){
	  val =  getParsedData(temp, std::string("VAL"),va);
	  va++;
	}
	
	i = temp.find("END");
	j = temp.find("BEGIN OBJECT");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*ty*co*hi*da*dod*d*we*sp*at*va==1){
	std::vector<std::string> arg;
	arg.push_back(name);
	arg.push_back(desc);
	arg.push_back(type);
	arg.push_back(color);
	arg.push_back(hit);
	arg.push_back(dam);
	arg.push_back(dodge);
	arg.push_back(def);
	arg.push_back(weight);
	arg.push_back(speed);
	arg.push_back(attr);
	arg.push_back(val);

	objectTypes.push_back(object(arg));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
      
      
      //std::cout<<"Checking Next Object"<<std::endl<<std::endl;
      std::getline(oloadfile,temp);
      
      
    }
    
    std::cout<<"Found "<<numcorrect<<" Objects and "<<numerrors << " Parsing Errors."<<std::endl<<std::endl;; 
    if(checkParse){
      std::cout<<"Checking Object Parsing."<<std::endl<<std::endl;
      std::vector<object>::iterator itr;
      
      for(itr=objectTypes.begin();itr!=objectTypes.end();itr++){
	(*itr).parseObject();
      }
    }
  }
  
  
  return 0;
}


int Game::parseFile(std::string mfilename, std::string ofilename){
  
  
  int parseM = parseMonsters(mfilename);
  int parseO = parseObjects(ofilename);
  
  if(parseM == -1 || parseO==-1){
    return !(parseM == -1 || parseO==-1);
  }
  
  objectTypes.front().setSym();
  
 return 1; 
}
void Game::generateMonsters(){
  bool monsterMap[MAX_V][MAX_H];
  memset(monsterMap,false,sizeof(monsterMap));
  character_map.clear();
  deleteMonsters();
  int i;
  std::vector<std::string> unique;
  for(i = 0; i<flag->_NumberMonsters;i++){ 
    
    
    mList.push_back(new monster_t());
    
    *(mList.back()) = monsterTypes.at(rand()%monsterTypes.size());
    bool check = std::find(unique.begin(),unique.end(),mList.back()->_name)!=unique.end();
    
    
    while(check || (!mList.back()->_isAlive && ((0x1<<7)&mList.back()->attr))){
      *(mList.back()) = monsterTypes.at(rand()%monsterTypes.size());
      check = std::find(unique.begin(),unique.end(),mList.back()->_name)!=unique.end();
    }
    
    if(((0x1<<7)&mList.back()->attr) && mList.back()->_isAlive){
      unique.push_back(mList.back()->_name);
      
    }
   
    
    ((monster_t*)mList.back())->setGame(console,p,d);
    
    if(((monster_t *)mList.back())->initMonsterPosition(monsterMap)){
      eventQueue.push_back(mList.back());
      monsterMap[mList.back()->getPos().y()][mList.back()->getPos().x()]=true;  
    }
    else{
      delete mList.back();
      mList.back() =NULL;
      mList.erase(mList.end()-1);
      break;
    }
  }
  
  std::vector<character_t*>::iterator m_itr;
  for(m_itr = mList.begin();m_itr!=mList.end();m_itr++){
    insertCharacter((*m_itr)->getPos(),*m_itr);
  }
  
  
}

void Game::generateObjects(){
 
  object_map.clear();
  bool onStairs = false;
  for(int i = 0; i<50;i++){
    cMapItr c_itr;
    object new_object; 
    do{
    new_object = objectTypes.at(rand()%objectTypes.size());
    new_object.setGame(console,d);
    new_object.setRandomPos();
    //Check if It lands on a character space.
    c_itr = insertCharacter(new_object.getPos(),NULL);
    if(c_itr.second){
      eraseCharacter(new_object.getPos());
    }
    else{
    }
    
    onStairs = d->checkStairs(new_object.getPos());
    
    
    } while(!c_itr.second || onStairs);
    insertObject(new_object.getPos(), new_object);
    
  }
  
  
}

void Game::renderObjects(){
  oMap::iterator itr;
  WINDOW* gameWind = console->getGameWindow();
  for(itr = object_map.begin();itr!=object_map.end();itr++){
    
    if((*itr).second.size()>1){
      int c_Index = getElapsedTime() % (*itr).second.size();
      wattron(gameWind,COLOR_PAIR((*itr).second.at(c_Index)._colorIndex.at(0)));
      mvwaddch(gameWind,(*itr).second.front().getPos().y(), (*itr).second.front().getPos().x(), '&');
      wattroff(gameWind,COLOR_PAIR((*itr).second.at(c_Index)._colorIndex.at(0)));
    }
    else{
      (*itr).second.front().renderObject();
    }
    
  }
}


void Game::renderMap(){
      int i, j;
      WINDOW* gameWind = console->getGameWindow();
      for(j = 0; j<MAX_V;j++){ 
	      for(i = 0; i<MAX_H;i++){
		      if(p->seenMap[j][i] == 0 || p->seenMap[j][i] == ' '){
			      mvwaddch(gameWind,j, i, ' ');
		      }
		      else{
			      //if(seenMap[j][i] == '.' || seenMap[j][i] == '#' || seenMap[j][i] == '<' || seenMap[j][i] == '' )
				
			      if((position_t(i,j)-p->getPos())*(position_t(i,j)-p->getPos()) <= p->_sight){
				if(p->checkLos(position_t(i,j))){
				  
				  if(p->seenMap[j][i] != '<' && p->seenMap[j][i] != '>'){
				  wattron(gameWind,COLOR_PAIR(9));
				  wattron(gameWind,A_BOLD);
				  mvwaddch(gameWind,j, i, p->seenMap[j][i]);
				  wattroff(gameWind,COLOR_PAIR(9));
				  }
				  else{
				  mvwaddch(gameWind,j, i, p->seenMap[j][i]);
				  }
				  
				  oMapItr o_itr = checkObject(position_t(i,j));
				  if(!o_itr.second){
				      if((o_itr).first->second.size()>1){
					int c_Index = getElapsedTime() % (o_itr).first->second.size();
					wattron(gameWind,COLOR_PAIR((o_itr).first->second.at(c_Index)._colorIndex.at(0)));
					mvwaddch(gameWind,j,i, '&');
					wattroff(gameWind,COLOR_PAIR((o_itr).first->second.at(c_Index)._colorIndex.at(0)));
				      }
				      else{
					(o_itr).first->second.front().renderObject();
				      }
				  }
				  else{
				    eraseObject(position_t(i,j));
				  }
				  
				  cMapItr c_itr = insertCharacter(position_t(i,j),NULL);
				  if(!c_itr.second){
				    c_itr.first->second->renderCharacter();
				  }
				  else{
				    eraseCharacter(position_t(i,j));
				  }
				  
				  wattroff(gameWind,A_BOLD);
				  
				}
				else{
				  mvwaddch(gameWind,j, i, p->seenMap[j][i]); 
				}
			      }
			      else{
				mvwaddch(gameWind,j, i, p->seenMap[j][i]);
			      }	     
		      }
	      }
	      wprintw(gameWind,"\n");
      }
      wprintw(gameWind,"\n");
}


void Game::renderMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    (*itr)->renderCharacter();
  } 
}

void Game::resetEventList(){
  eventQueue.clear();
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
   eventQueue.push_back((*itr));
  }
}

void Game::gameInit(){
  
  initscr();
  console = new console_t();
  raw();
  noecho();
  curs_set(0);
  start_color();
  initAllColors();
  _reveal = false;
  teleporting = false;
  displayMonsterList = false;
  startTime = std::chrono::system_clock::now();
  d = new Dungeon(console,flag->_Max_Rooms, flag->_save, flag->_load, flag->_filename);
  p = new player_t(console, d, flag->_player_speed);
  
  p->setEventTime(0);
  eventQueue.push_back(p);
  
  
  mList_W = MAX_H*3/4;
  mList_H = MAX_V*3/4;
  monsterList = newwin(mList_H,mList_W,(MAX_V-mList_H)/2,(MAX_H-mList_W)/2);
 
  generateMonsters();
  insertCharacter(p->getPos(),p);
  generateObjects();
}


void Game::listInput(){
    displayMonsterList = true;
    listStart = 0;
    int size = mList.size();
    int list_size = mList_H;    
    int key = 0;
    
    keypad(monsterList,TRUE);
    
  while(key!=27 && key!='Q'){    
    
    key = wgetch(monsterList);
    if(key == KEY_UP){  
      if(listStart>0){
	listStart--;
      }
    }
    
    if(key == KEY_DOWN){
      if((listStart+list_size)<size){
	listStart++;
      }
    }  
  }
  displayMonsterList = false;
}


void Game::printMonsterList(){
  
  int i;
  char lat[][6] = {"North", "", "South"};
  char lon[][6] = {"West", "", "East"};
  int list_size = mList_H;
  
    
    set_escdelay(0);
    wclear(monsterList);
    //std::vector<character_t*>::iterator itr=mList.begin();
    cMap::iterator itr = character_map.begin();
    for(i=0; i<listStart;i++){
      itr++;
    }
    
    for(i=listStart; i<(listStart+list_size) && itr!=character_map.end();i++){
	if((0x1<<7)&((*itr).second->attr)){
	 wattron(monsterList, COLOR_PAIR(COLOR_RED+10)); 
	}
	  wprintw(monsterList, "%d %s\t", i,(*itr).second->_name.c_str() );
	  position_t dp = ((*itr).second->getNPos())-(p->getNPos());
	if((dp.x()<0?-1:dp.x()>0)!=0){
	  wprintw(monsterList, "%d %s",abs(dp.x()),lon[(dp.x()<0?-1:dp.x()>0) +1]);
	  if((dp.y()<0?-1:dp.y()>0)!=0){
	    wprintw(monsterList, ", ");
	  }
	}
	if((dp.y()<0?-1:dp.y()>0)!=0){
	  wprintw(monsterList, "%d %s\n",abs(dp.y()),lat[(dp.y()<0?-1:dp.y()>0)+1]);
	}
	else{
	  wprintw(monsterList,"\n");  
	}
	wattroff(monsterList, COLOR_PAIR(COLOR_RED+10)); 
	itr++;
    }
  wrefresh(monsterList);
  
  
  
}

void Game::teleportMode(){
  teleporting = true;
  int key = 0;
  //char teleSym = '*';
  teleport = p->getPos();
  //WINDOW* gameWind = console->getGameWindow();
  
  //Render();
  //mvwaddch(gameWind,teleport.y(),teleport.x(),teleSym);
  
  while(key!='t' && key!='r'){
      position_t newTeleport = position_t(0,0);
      //console->print_info(std::string("Entered Teleport Mode. t to teleport, r to randomly teleport."));
      while((!p->validKeyCheck(key) && (key!='r')) || ((newTeleport.x()<1 || newTeleport.x()>MAX_H-2 || newTeleport.y()<1 || newTeleport.y()>MAX_V-2))){
	
	//console->refresh();
	flushinp();
	key = wgetch(console->getGameWindow());
	if(key =='r'){
	  teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  while(d->checkHardness(teleport)){
	    teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  }
	}
	newTeleport = teleport + getnewDirection(p->translateKeyinput(key));	  
      }
 
      teleport = newTeleport;
      //Render();
      //mvwaddch(gameWind,teleport.y(),teleport.x(),teleSym);
  }
  teleporting = false;
  p->npos = teleport;
}



void Game::resetMap(){
  
  d->createDungeon();
  generateMonsters();
  resetEventList();
  generateObjects();
  
}

void Game::Render(){
  
    while(Rendering){
    //console->clear();
    
    //Render All Console;
      if(!displayMonsterList){
      if(_reveal || teleporting){
	d->renderDungeon();
	renderObjects();
	renderMonsters();
	wattron(console->getGameWindow(), A_BLINK);
	p->renderCharacter();
	wattroff(console->getGameWindow(), A_BLINK);
	if(teleporting){
	  //Render Cursor
	  char teleSym = '*';
	  wattron(console->getGameWindow(), A_BLINK);
	  mvwaddch(console->getGameWindow(),teleport.y(),teleport.x(),teleSym);
	  wattroff(console->getGameWindow(), A_BLINK);
	}
	
      }
      else{
	renderMap();
	wattron(console->getGameWindow(), A_BLINK);
	p->renderCharacter();
	wattroff(console->getGameWindow(), A_BLINK);
      }
      console->refresh();
      }
      else{
	printMonsterList();
      }
      usleep(1000);
    }
}


void Game::gameLoop(){
  int key = 0;
  int count = 0;
  Rendering = true;
  
    //Render();
    std::thread renderThread(&Game::Render,this);
    
    std::make_heap(eventQueue.begin(),eventQueue.end(),character_t());
    std::sort_heap(eventQueue.begin(),eventQueue.end(),character_t());  
  while(key!=27 && key!='Q' && p->getAlive() && mList.size()){
  
    
    //Clear Everything in Console Before Rendering
    
    std::make_heap(eventQueue.begin(),eventQueue.end(),character_t());
    std::sort_heap(eventQueue.begin(),eventQueue.end(),character_t());
    //Get Next Character
    character_t* character = eventQueue.front();
    std::pop_heap(eventQueue.begin(),eventQueue.end());
    eventQueue.pop_back();
    
    if(!character){
      continue;
    }
    
    
    std::vector<character_t *>::iterator itr;
    for(itr=eventQueue.begin();itr!=eventQueue.end();itr++){
      (*itr)->decreaseEventTime(character->getEventTime());
    }
    
    
    
    key = 0;
    int waitTime = character->getEventTime(); 
    
    
    if(character->getSym() == '@'){
      //Handle Player Behaviour
      eraseCharacter(character->getPos());
      p->setEventTime();
      key = p->playerCommand(key);
      mtx.lock();
      if(key == '<' || key == '>'){
	//Reset Map
	if(d->checkMap(p->getPos()) == '<' && key=='<'){
	  resetMap();
	  p->setPos(d->getStairs(1));
	  p->resetMap();
	}
	if(d->checkMap(p->getPos()) == '>'&& key=='>'){
	  resetMap();
	  p->setPos(d->getStairs(0));
	  p->resetMap();
	}
	p->setEventTime(0);
      }
      
      if(key == 'm'){
	//Render Monster List
	
	listInput();
	p->setEventTime(0);  
	displayMonsterList = false;
      }
      
      if(key == 't'){
	//Teleport Mode
	teleportMode();
	p->setEventTime(0);
	  
	
      }
      if(key == 'f'){
	_reveal = !_reveal;
	p->setEventTime(0);
      }
      //Check if monster next space
      cMapItr mCheck = insertCharacter(p->getNPos(),p);
      
      if(!mCheck.second){
	std::vector<character_t*>::iterator mitr;
	mitr = std::find(mList.begin(),mList.end(), mCheck.first->second);
	if((*mitr)->attr & 0x1<<7){
	  std::vector<monster_t>::iterator t_itr;
	  for(t_itr = monsterTypes.begin();t_itr!=monsterTypes.end();t_itr++){
	    if(!(*mitr)->_name.compare((*t_itr)._name)){
	      (*t_itr)._isAlive = false;
	    }
	    
	  }
	  
	  
	}
	eraseCharacter((*mitr)->getPos());
	delete (*mitr);
	(*mitr) =NULL;
	mList.erase(mitr);
	resetEventList();
      }
     
      //Move the Player
      p->movePlayer();
      insertCharacter(p->getPos(), p);
      eventQueue.push_back(p);
      std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
      mtx.unlock();
    }
    else{
	mtx.lock();
	monster_t* m = dynamic_cast<monster_t*>(character);
	if(m){
	  eraseCharacter(m->getPos());
	  
	  m->monsterNextMove();
	  
	  //Check If Player
	  if(m->npos== p->getPos()){
	    p->setAlive(false);
	  }
	  
	  
	  cMapItr mCheck = insertCharacter(m->getNPos(),m);
	  //Check If Monster
	  if(!mCheck.second){
	    m->npos = m->pos;
	  }
	  else{
	  }
	  eraseCharacter(m->getNPos());
	  m->moveMonster();
	  insertCharacter(m->getPos(),m);
	  //key = wgetch(console->getGameWindow());
	  m->setEventTime();
	  eventQueue.push_back(m);
	  std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
	}
	mtx.unlock();
    }

     //Render();
    
    count++;
    
      if(waitTime> 0){ 
	usleep(waitTime*10);
      }
  }
  
  Rendering = false;
  renderThread.join();
  GameOverScreen();
}



void Game::GameOverScreen(){
    console->clear();
    d->renderDungeon();
    renderObjects();
    renderMonsters();
    p->renderCharacter();
    
    if(mList.size()){
      mvwprintw(console->getInfoWindow(),0,0,"You Died: Monsters Left %d",mList.size());
    }
    else{
      mvwprintw(console->getInfoWindow(),0,0,"You Win.");
    }
      mvwprintw(console->getInfoWindow(),1,0,"Press Q to quit.");
     
    console->refresh();
    
    while(wgetch(console->getGameWindow())!='Q');
}


void Game::initAllColors(){
  /*
  init_pair(1, COLOR_BLACK, COLOR_YELLOW);//Los background
  init_pair(18, COLOR_BLACK, COLOR_BLUE);//Fog background
  
  init_pair(2, COLOR_RED,COLOR_BLACK); //Erratic
  init_pair(3, COLOR_YELLOW,COLOR_BLACK); 
  init_pair(4, COLOR_GREEN,COLOR_BLACK);
  init_pair(5, COLOR_BLUE,COLOR_BLACK); //Intelligent
  init_pair(6, COLOR_CYAN,COLOR_BLACK);
  init_pair(7, COLOR_MAGENTA,COLOR_BLACK);

  init_pair(12, COLOR_RED,COLOR_YELLOW); //Erratic
  init_pair(13, COLOR_YELLOW,COLOR_YELLOW); 
  init_pair(14, COLOR_GREEN,COLOR_YELLOW);
  init_pair(15, COLOR_BLUE,COLOR_YELLOW); //Intelligent
  init_pair(16, COLOR_CYAN,COLOR_YELLOW);
  init_pair(17, COLOR_MAGENTA,COLOR_YELLOW);
  init_pair(8, COLOR_WHITE,COLOR_RED);  
*/
  
  init_pair(COLOR_RED, COLOR_RED,COLOR_BLACK);
  init_pair(COLOR_YELLOW, COLOR_YELLOW,COLOR_BLACK);
  init_pair(COLOR_GREEN, COLOR_GREEN,COLOR_BLACK);
  init_pair(COLOR_BLUE, COLOR_BLUE,COLOR_BLACK);
  init_pair(COLOR_CYAN, COLOR_CYAN,COLOR_BLACK);
  init_pair(COLOR_MAGENTA, COLOR_MAGENTA,COLOR_BLACK);
  init_pair(COLOR_WHITE, COLOR_WHITE,COLOR_BLACK);
  init_pair(COLOR_BLACK, COLOR_BLACK,COLOR_WHITE);
  
  init_pair(9, COLOR_BLACK, COLOR_YELLOW);//Los background
  init_pair(10, COLOR_BLACK, COLOR_BLUE);//Fog background
  init_pair(COLOR_RED+10, COLOR_BLACK,COLOR_RED);
  init_pair(COLOR_YELLOW+10, COLOR_BLACK,COLOR_YELLOW);
  init_pair(COLOR_GREEN+10, COLOR_BLACK,COLOR_GREEN);
  init_pair(COLOR_BLUE+10, COLOR_BLACK,COLOR_BLUE);
  init_pair(COLOR_CYAN+10, COLOR_BLACK,COLOR_CYAN);
  init_pair(COLOR_MAGENTA+10, COLOR_BLACK,COLOR_MAGENTA);
  init_pair(COLOR_WHITE+10, COLOR_BLACK,COLOR_WHITE);
  init_pair(COLOR_BLACK+10, COLOR_BLACK,COLOR_BLACK);
  
}


Game::cMapItr Game::insertCharacter(position_t pos, character_t* c){
  return character_map.insert(cPair(pos.getKey(),c));
}

void Game::eraseCharacter(position_t pos){
  character_map.erase(pos.getKey());
}


Game::oMapItr Game::checkObject(position_t pos){
  oMapItr itr = object_map.insert(oPair(pos.getKey(),std::vector<object>()));
  return itr;
}
Game::oMapItr Game::insertObject(position_t pos, object obj){
  oMapItr itr = object_map.insert(oPair(pos.getKey(),std::vector<object>()));
  obj.setPos(pos);
  itr.first->second.push_back(obj);
  return itr;
}


std::vector<object> Game::eraseObject(position_t pos){
  std::vector<object> objects = object_map.at(pos.getKey());
  object_map.erase(pos.getKey());
  return objects;
}
