#include "player.h"

player_t::player_t(){
}

player_t::~player_t(){
}

player_t::player_t(console_t* con, Dungeon* dungeon, int spd) : player_t(){
  console = con;
  _isAlive = true;
  d = dungeon;
  _sight = 16;
  resetMap();
  setSym('@');
  setRandomPos();
  speed = spd;
  setEventTime();
}

void player_t::resetMap(){
  memset(seenMap, ' ',sizeof(char)*MAX_H*MAX_V);
}
void player_t::setRandomPos(){
  
  std::vector<Room> Rooms = d->getRooms();
  int randomRoom = rand()%Rooms.size();
  pos = Rooms.at(randomRoom).getRandomPosition();
  npos = pos;
  movePlayer();
}

int player_t::playerCommand(int k){
  int key = k;
  while(!validKeyCheck(key) || (npos.x()<1 || npos.x()>(MAX_H-2) || npos.y()< 1 || npos.y() >(MAX_V-2))){
	flushinp();
	key = wgetch(console->getGameWindow());
		
	npos = pos + getnewDirection(translateKeyinput(key));
	
	if(d->checkHardness(npos)!=0){
	  if(key!='t' && key!='f' && key!='5' && key!=' ' && key!='Q'){
	    key = 0;
	  }
	}
  }
  return key;
}




int player_t::movePlayer(){
  pos = npos;
  d->createTunnelMap(pos);
  d->createNonTunnelMap(pos);
  
  for(int i =-3;i<4;i++){
      for(int j=-3; j<4;j++){
	position_t sPos = pos + position_t(i,j);
	position_t delta_pos = sPos - pos;
	if(checkLos(sPos) && delta_pos*delta_pos <=_sight){
	  seenMap[sPos.y()][sPos.x()] = d->getSym(sPos);
	}
      }
  }
  
  return 1;
}

void player_t::teleportMode(std::vector<character_t*> mList){
  int key = 0;
  char teleSym = '*';
  position_t teleport = pos;
  WINDOW* gameWind = console->getGameWindow();
  
  
  
      d->renderDungeon();
      
      std::vector<character_t*>::iterator itr;
      for(itr = mList.begin();itr!=mList.end();itr++){
	(*itr)->renderCharacter();
      }
      
      renderCharacter();
      mvwaddch(gameWind,teleport.y(),teleport.x(),teleSym);
  
  
  while(key!='t' && key!='r'){
      position_t newTeleport = position_t(0,0);
      console->print_info(std::string("Entered Teleport Mode. t to teleport, r to randomly teleport."));
      while((!validKeyCheck(key) && (key!='r')) || ((newTeleport.x()<1 || newTeleport.x()>MAX_H-2 || newTeleport.y()<1 || newTeleport.y()>MAX_V-2))){
	
	console->refresh();
	flushinp();
	key = wgetch(console->getGameWindow());
	if(key =='r'){
	  teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  while(d->checkHardness(teleport)){
	    teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  }
	}
	newTeleport = teleport + getnewDirection(translateKeyinput(key));	  
      }
 
      teleport = newTeleport;
      
      d->renderDungeon();
      
      std::vector<character_t*>::iterator itr;
      for(itr = mList.begin();itr!=mList.end();itr++){
	(*itr)->renderCharacter();
      }
      
      renderCharacter();
      mvwaddch(gameWind,teleport.y(),teleport.x(),teleSym);
  }
  npos = teleport;
}


bool player_t::checkLos(position_t check){
  position_t checkPos = pos;
  
      
  while(!(checkPos==check)){
    int dx = check.x() - checkPos.x();
    int dy = check.y() - checkPos.y();
    dx = dx < 0 ? -1: dx > 0;
    dy = dy < 0 ? -1: dy > 0;
    checkPos = checkPos + position_t(dx,dy);
    if(d->checkHardness(checkPos)!=0){
      return false;
    }
  }
  
  return true;
}


bool player_t::validKeyCheck(int key){
  char check[] = "ftm<>123456789Qykulnjbh ";
  int size = strlen(check);
  int i; 
  for(i = 0; i<size;i++){
    if(check[i]==(unsigned char)(key)){
      return true;
    }
  }
  return false;
}

int player_t::translateKeyinput(int key){
  int val = 9;
  switch(key){
    case 49:
      val = 0;
      break;
    case 50:
      val = 1;
      break;
    case 51:
      val = 2;
      break;
    case 52:
      val = 7;
      break;
    case 54:
      val = 3;
      break;
    case 55:
      val = 6;
      break;
    case 56:
      val = 5;
      break;
    case 57:
      val = 4;
      break;
    case 'b':
      val = 0;
      break;
    case 'j':
      val = 1;
      break;
    case 'n':
      val = 2;
      break;
    case 'h':
      val = 7;
      break;
    case 'l':
      val = 3;
      break;
    case 'y':
      val = 6;
      break;
    case 'k':
      val = 5;
      break;
    case 'u':
      val = 4;
      break;
      
    default:
      break;
  }

  return val;
}